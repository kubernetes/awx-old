import pytest
import requests


def test_pods(kind_cluster):
    kind_cluster.kubectl("apply", "-f", "manifests")
    # kind_cluster.kubectl("create", "secret", "-n", "awx", "generic", "awx-secrets", "--from-literal=credentials_py=test", "--from-literal=environment_sh=test")
    # kind_cluster.kubectl("create", "secret", "-n", "awx", "generic", "postgres-password", "--from-literal=MYPASSWORD=test")

    # assert kind_cluster.api.version == ('1', '16')
    with kind_cluster.port_forward("service/awx", 80, '-n', 'awx') as port:
        r = requests.get(f"http://localhost:{port}/api/v2/ping/")
        r.raise_for_status()
        assert r.json()['instances']
